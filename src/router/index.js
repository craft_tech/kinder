import Vue from 'vue'
import Router from 'vue-router'
// import main from '@/components/main'
// import home from '@/components/home'
// import loading from '@/components/loading'
// import choose from '@/components/choose'
// import nchoose from '@/components/nchoose'
// import userinfo from '@/components/userinfo'
// import gravity from '@/components/gravity'
// import result from '@/components/result'
// import poster from '@/components/poster'


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'main',
      component: resolve => {
        require(['@/components/main.vue'], resolve)
      }, //懒加载
      redirect:'home',
      children:[
        {
          path: '/home',
          name: 'home',
          component: resolve => {
            require(['@/components/home.vue'], resolve)
          }, //懒加载
        },  
        {
          path: '/choose',
          name: 'choose',
          component: resolve => {
            require(['@/components/choose.vue'], resolve)
          }, //懒加载
        },
        {
          path: '/userinfo',
          name: 'userinfo',
          component: resolve => {
            require(['@/components/userinfo.vue'], resolve)
          }, //懒加载
        }
      ]
    },
    {
      path: '/loading',
      name: 'loading',
      component: resolve => {
        require(['@/components/loading.vue'], resolve)
      }, //懒加载
    },
    {
      path: '/nchoose',
      name: 'nchoose',
      component: resolve => {
        require(['@/components/nchoose.vue'], resolve)
      }, //懒加载
    },
    {
      path: '/gravity',
      name: 'gravity',
      component: resolve => {
        require(['@/components/gravity.vue'], resolve)
      }, //懒加载
    },{
      path: '/result',
      name: 'result',
      component: resolve => {
        require(['@/components/result.vue'], resolve)
      }, //懒加载
    }
    // ,{
    //   path: '/poster',
    //   name: 'poster',
    //   component: resolve => {
    //     require(['@/components/poster.vue'], resolve)
    //   }, //懒加载
    // }
  ]
})
