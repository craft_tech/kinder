import wx from 'weixin-js-sdk'


console.log(wx_conf)
wx.config({
    debug: false,
    appId: wx_conf.appId,
    timestamp: wx_conf.timestamp,
    nonceStr: wx_conf.nonceStr,
    signature: wx_conf.signature,
    jsApiList: [
        'checkJsApi',
        'onMenuShareTimeline',
        'onMenuShareAppMessage'
    ]
});
//分享标题
var wxtitle = '健达奇趣蛋邀请你和宝贝出发！趣探索';
//分享内容
var wxdesc = '健达奇趣蛋邀请你和国家地理摄影师探索世界';

var shareurl = 'https://kinderjoy.gypserver.com/wechat';//正式

//分享图片
var imgUrl = 'https://kinderjoy.gypserver.com/wechat/static/img/share.jpg';//正式

//var shareurl = 'https://kinderjoytest.gypserver.com/demo/wx'//测试

//var imgUrl = 'https://kinderjoytest.gypserver.com/demo/wx/static/img/share.jpg';//测试


wx.ready(function() {
    var shareData = {
        title: wxtitle,
        desc: wxdesc,
        link: shareurl,
        imgUrl: imgUrl,
        success: function() { 
            gtag('event', 'share_friend', {'event_category': 'kinder','event_label': 'click'});
        },
        error: function() {}
    };
    var shareDataline = {
        title: wxtitle,
        desc: wxdesc,
        link: shareurl,
        imgUrl: imgUrl,
        success: function() { 
            gtag('event', 'share_moment', {'event_category': 'kinder','event_label': 'click'});   
        },
        error: function() {}
    };
    wx.onMenuShareAppMessage(shareData);
    wx.onMenuShareTimeline(shareDataline);

});