// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import 'amfe-flexible/index.js'
import wxapi from "./common/libs/wxapi"

Vue.config.productionTip = false

// import Vant from 'vant';
import 'vant/lib/index.css';

// import { Swipe, SwipeItem } from 'vant';
// Vue.use(Swipe).use(SwipeItem);
// Vue.use(Vant);
import { Toast } from 'vant';
Vue.use(Toast);

// import Vconsole from 'vconsole'
// let vConsole = new Vconsole()
// Vue.use(vConsole)


/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>',
  data(){
      return{
        bgmiconUrl:'static/img/bgmicon.png',
        isplay:false,
        showICON:true
      }
  }
})
